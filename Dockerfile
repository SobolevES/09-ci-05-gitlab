FROM centos:7

RUN yum update -y
RUN yum groupinstall "Development Tools" -y
RUN yum install openssl-devel libffi-devel bzip2-devel yum install python-devel curl -y

RUN mkdir -p /tmp/python39 && \
    curl -SL https://www.python.org/ftp/python/3.9.6/Python-3.9.6.tar.xz \
    | tar -xJC /tmp/python39 && \
    cd /tmp/python39/Python-3.9.6 && \
    ./configure --enable-optimizations && \
    make altinstall && \
    rm -rf /tmp/python39/Python-3.9.6.tar.xz

RUN curl https://bootstrap.pypa.io/get-pip.py --output get-pip.py && \
    python3.9 get-pip.py


RUN pip3 install flask flask-jsonpify flask-restful

ADD python-api.py /python-api/
ENTRYPOINT [ "python3.9", "/python-api/python-api.py" ]
